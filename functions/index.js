// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
// Scheduled function - https://github.com/firebase/functions-samples/tree/Node-8/delete-unused-accounts-cron
// Email function - https://github.com/firebase/functions-samples/tree/Node-8/email-confirmation
// 
//

const functions = require('firebase-functions');
const nodemailer = require('nodemailer');
const secureCompare = require('secure-compare');
const admin = require('firebase-admin');
admin.initializeApp();

const db = admin.firestore();


// Configure the email transport using the default SMTP transport and a GMail account.
// For other types of transports such as Sendgrid see https://nodemailer.com/transports/
const gmailEmail = functions.config().gmail.email;
const gmailPassword = functions.config().gmail.password;
const mailTransport = nodemailer.createTransport({
  service: 'gmail',
  auth: {
    user: gmailEmail,
    pass: gmailPassword,
  },
});

// https function to generate and send email with user data
exports.emailReportSend = functions.https.onRequest((request, response) => {

    const key = request.query.key;

    // Exit if the keys don't match.
    if (!secureCompare(key, functions.config().cron.key)) {
        console.log('The key provided in the request does not match the key set in the environment. Check that', key, 'matches the cron.key attribute in `firebase env:get`');
        return response.status(403).send();
    }
    else {

        var usersRef = db.collection('users');
        return usersRef.where('siteId', '<', 'TEST').orderBy('siteId').get()
          .then(snapshot => {

             var contents = 'Signed Up,Site,Code,Email,Question 1,Question 2,Question 3,Question 4,Question 5\n';

             snapshot.forEach(doc => {
                //console.log(doc.id, '=>', doc.data());

                contents += doc.createTime.toDate().toLocaleDateString("en-US");
                contents += ',';
                contents += doc.data().siteId;
                contents += ',';
                contents += doc.data().codeId; 
                contents += ',';
                contents += doc.data().email;  

                if (doc.data().surveys !== null) {
                  for (var key in doc.data().surveys) {
                    if (doc.data().surveys.hasOwnProperty(key)) {
                        var value = doc.data().surveys[key];
                        contents += ',';
                        contents += value.value === null ? "" : value.value; 
                    }
                  }
                }

                contents += '\n';
             });

             ////console.log(contents);

             var today = new Date();
             
             var filename = 'Weekly RHCP S4C Report ' + today.toISOString().substr(0, 10) + '.csv';
             var bucket = admin.storage().bucket();
             var file = bucket.file('reports/' + filename);
             const filecontents = contents;
     
             file.save(filecontents, err => {
                 if (err) {
                     console.log("Error", err);
                 }

                 return;
             });

             const email = 's4c@mayo.edu'
             ////const email = 'eric@norsemendigital.com'

             return response.status(200).send(sendReportEmail(email, filename, `RHCP Stories 4 Change Weekly Report`, filecontents));
          })
          .catch(err => {
            console.log("Error", err);
            return response.status(500).send(err);
          });
    }
});

// https function to generate and send email with reporting data
exports.emailReportEventsSend = functions.https.onRequest((request, response) => {

  const key = request.query.key;

  // Exit if the keys don't match.
  if (!secureCompare(key, functions.config().cron.key)) {
      console.log('The key provided in the request does not match the key set in the environment. Check that', key, 'matches the cron.key attribute in `firebase env:get`');
      return response.status(403).send();
  }
  else {

      var usersRef = db.collection('reporting');
      //return usersRef.orderBy('siteId').orderBy('studyCode').orderBy('eventTime').get()
      return usersRef.where('siteId', '<', 'RCH').orderBy('siteId').orderBy('studyCode').orderBy('eventTime').get()
        .then(snapshot => {

           var contents = 'Site,Study Code,Date,Type,Name\n';

           snapshot.forEach(doc => {
              contents += doc.data().siteId;
              contents += ',';
              contents += doc.data().studyCode; 
              contents += ',';
              contents += doc.data().eventTime.substr(0,10)
              contents += ',';
              contents += doc.data().itemType;  
              contents += ',';
              contents += doc.data().itemName;
              contents += '\n';
           });

           var today = new Date();
           
           var filename = 'Weekly RHCP S4C Events Report ' + today.toISOString().substr(0, 10) + '.csv';
           var bucket = admin.storage().bucket();
           var file = bucket.file('reports/' + filename);
           const filecontents = contents;
   
           file.save(filecontents, err => {
               if (err) {
                   console.log("Error", err);
               }

               return;
           });

           const email = 's4c@mayo.edu'
           ////const email = 'eric@norsemendigital.com'

           return response.status(200).send(sendReportEmail(email, filename, `RHCP Stories 4 Change Weekly Events Report`, filecontents));
        })
        .catch(err => {
          console.log("Error", err);
          return response.status(500).send(err);
        });
  }
});


/**
 * When a user views a video or education
 */
exports.trackContentViews = functions.analytics.event('select_content').onLog((event) => {

  return db.collection("reporting").add({
    userId: event.user.userId,
    studyCode: event.user.userProperties["study_code"].value,
    siteId: event.user.userProperties["site_id"].value,
    itemName: String(event.params["item_name"]),
    itemId: String(event.params["item_id"]),
    itemType: String(event.params["content_type"]),
    eventDate: event.reportingDate,
    eventTime: event.logTime,
    eventName: "select_content"
  }).then(() => {
    return console.log("Successfully saved for " + event.user.userId);
  });
});

function sendReportEmail(email, filename, subject, contents) {

    const mailOptions = {
      from: `RHCP Stories 4 Change <rhcps4c@gmail.com>`,
      to: email
    };

    if (contents !== '') {
        mailOptions.attachments = [
            {
                filename: filename,
                content: contents,
                contentType: 'text/plain'
            }
        ];
    }

    mailOptions.subject = subject;
    mailOptions.text = ``;
    
    return mailTransport.sendMail(mailOptions).then(() => {
      return console.log('Weekly report mail sent to ' + email);
    });
  }